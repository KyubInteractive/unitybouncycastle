[VERSION 1.8.6]
Defects Fixed
- EdDSA verifiers now reject overly long signatures.
- Fixed field reduction for custom secp128r1 curve.
- ASN.1: Enforce no leading zeroes in OID branches (longer than 1 character).

Additional Features and Functionality
- TLS: BasicTlsPskIdentity now reusable (returns cloned array from GetPsk).
- Improved performance for multiple ECDSA verifications using same public key.
- Support has been added for ChaCha20-Poly1305 AEAD mode from RFC 7539.
- PKCS12: Improved support for certificate-only key stores without password.