﻿/**
 * 
 * This class represents the DTLS SRTP transport connection to use as Client or Server.
 * 
 * 
 * @author Rafael Soares (raf.csoares@kyubinteractive.com)
 * @improved by Aaron Clauson
 * 
 *
 */

using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.Security;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Org.BouncyCastle.Crypto.DtlsSrtp
{
    public class DtlsSrtpTransport : DatagramTransport
    {
        #region Consts

        public const int DEFAULT_MTU = 1500;
        public const int MIN_IP_OVERHEAD = 20;
        public const int MAX_IP_OVERHEAD = MIN_IP_OVERHEAD + 64;
        public const int UDP_OVERHEAD = 8;
        public const int MAX_DELAY = 20000;

        #endregion

        #region Private Variables

        private IPacketTransformer _srtpEncoder;
        private IPacketTransformer _srtpDecoder;
        private IPacketTransformer _srtcpEncoder;
        private IPacketTransformer _srtcpDecoder;
        IDtlsSrtpPeer _connection = null;

        MemoryStream _inStream = new MemoryStream();

        //Socket to receive
        Socket _socketContext = null;
        //Socket to send
        IPEndPoint _remoteEndPoint = null;

        public Action<byte[]> OnDataReady;

        private System.DateTime startTime = System.DateTime.MinValue;
        System.Random random = new Random();

        // Network properties
        private int mtu;
        private int receiveLimit;
        private int sendLimit;

        private volatile bool handshakeComplete;
        private volatile bool handshakeFailed;
        private volatile bool handshaking;

        #endregion

        #region Constructors

        public DtlsSrtpTransport(IDtlsSrtpPeer connection, int mtu = DEFAULT_MTU)
        {
            // Network properties
            this.mtu = mtu;
            this.receiveLimit = System.Math.Max(0, mtu - MIN_IP_OVERHEAD - UDP_OVERHEAD);
            this.sendLimit = System.Math.Max(0, mtu - MAX_IP_OVERHEAD - UDP_OVERHEAD);

            this._connection = connection;
        }

        #endregion

        #region Properties

        public IPacketTransformer SrtpDecoder
        {
            get
            {
                return _srtpDecoder;
            }
        }

        public IPacketTransformer SrtpEncoder
        {
            get
            {
                return _srtpEncoder;
            }
        }

        public IPacketTransformer SrtcpDecoder
        {
            get
            {
                return _srtcpDecoder;
            }
        }

        public IPacketTransformer SrtcpEncoder
        {
            get
            {
                return _srtcpEncoder;
            }
        }

        #endregion

        #region Public Handshake Functions

        public bool IsHandshakeComplete()
        {
            return handshakeComplete;
        }

        public bool IsHandshakeFailed()
        {
            return handshakeFailed;
        }

        public bool IsHandshaking()
        {
            return handshaking;
        }

        /// <summary>
        /// Used to know if this implementation is using socket or MemoryStream
        /// </summary>
        public bool HasSocketContext()
        {
            return _socketContext != null;
        }

        /// <summary>
        /// Perform handshake based in socket IPV6 Multiplexed and a RemoteEndPoint (take care to not read data from socket during handshake).
        /// If another part of your code read from socket provided, avoid use this function. Instead use parameteless version and provide data manually using WriteToRecvStream(buf)
        /// </summary>
        public bool DoHandshake(Socket socket, IPEndPoint targetEndPoint = null)
        {
            SetSocketContent(socket, targetEndPoint);
            if (_connection.IsClient())
            {
                return DoHandshakeAsClient();
            }
            else
            {
                return DoHandshakeAsServer();
            }
        }

        /// <summary>
        /// Perform handshake based in memory stream. Require provide data using WriteToRecvStream(buf)
        /// </summary>
        public bool DoHandshake()
        {
            return DoHandshake(null, null);
        }

        #endregion

        #region Internal Handshake Functions

        protected virtual bool DoHandshakeAsClient()
        {
            if (!handshaking && !handshakeComplete)
            {
                this.startTime = System.DateTime.Now;
                this.handshaking = true;
                SecureRandom secureRandom = new SecureRandom();
                DtlsClientProtocol clientProtocol = new DtlsClientProtocol(secureRandom);
                try
                {
                    var client = (DtlsSrtpClient)_connection;
                    // Perform the handshake in a non-blocking fashion
                    clientProtocol.Connect(client, this);

                    // Generate encoders for DTLS traffic
                    if (client.GetSrtpPolicy() != null)
                    {
                        _srtpDecoder = GenerateRtpDecoder();
                        _srtpEncoder = GenerateRtpEncoder();
                        _srtcpDecoder = GenerateRtcpDecoder();
                        _srtcpEncoder = GenerateRtcpEncoder();
                    }
                    // Declare handshake as complete
                    handshakeComplete = true;
                    handshakeFailed = false;
                    handshaking = false;
                    return true;
                }
                catch (System.Exception)
                {
                    // Declare handshake as failed
                    handshakeComplete = false;
                    handshakeFailed = true;
                    handshaking = false;
                }
            }
            return false;
        }

        protected virtual bool DoHandshakeAsServer()
        {
            if (!handshaking && !handshakeComplete)
            {
                this.startTime = System.DateTime.Now;
                this.handshaking = true;
                SecureRandom secureRandom = new SecureRandom();
                DtlsServerProtocol serverProtocol = new DtlsServerProtocol(secureRandom);
                try
                {
                    var server = (DtlsSrtpServer)_connection;
                    // Perform the handshake in a non-blocking fashion
                    serverProtocol.Accept(server, this);

                    // Generate encoders for DTLS traffic
                    if (server.GetSrtpPolicy() != null)
                    {
                        _srtpDecoder = GenerateRtpDecoder();
                        _srtpEncoder = GenerateRtpEncoder();
                        _srtcpDecoder = GenerateRtcpDecoder();
                        _srtcpEncoder = GenerateRtcpEncoder();
                    }
                    // Declare handshake as complete
                    handshakeComplete = true;
                    handshakeFailed = false;
                    handshaking = false;
                    return true;
                }
                catch (System.Exception)
                {
                    // Declare handshake as failed
                    handshakeComplete = false;
                    handshakeFailed = true;
                    handshaking = false;
                }
            }
            return false;
        }

        protected byte[] GetMasterServerKey()
        {
            return _connection.GetSrtpMasterServerKey();
        }

        protected byte[] GetMasterServerSalt()
        {
            return _connection.GetSrtpMasterServerSalt();
        }

        protected byte[] GetMasterClientKey()
        {
            return _connection.GetSrtpMasterClientKey();
        }

        protected byte[] GetMasterClientSalt()
        {
            return _connection.GetSrtpMasterClientSalt();
        }

        protected SrtpPolicy GetSrtpPolicy()
        {
            return _connection.GetSrtpPolicy();
        }

        protected SrtpPolicy GetSrtcpPolicy()
        {
            return _connection.GetSrtcpPolicy();
        }

        protected IPacketTransformer GenerateRtpEncoder()
        {
            return GenerateTransformer(_connection.IsClient(), true);
        }

        protected IPacketTransformer GenerateRtpDecoder()
        {
            //Generate the reverse result of "GenerateRtpEncoder"
            return GenerateTransformer(!_connection.IsClient(), true);
        }

        protected IPacketTransformer GenerateRtcpEncoder()
        {
            var isClient = _connection is DtlsSrtpClient;
            return GenerateTransformer(_connection.IsClient(), false);
        }

        protected IPacketTransformer GenerateRtcpDecoder()
        {
            //Generate the reverse result of "GenerateRctpEncoder"
            return GenerateTransformer(!_connection.IsClient(), false);
        }

        protected IPacketTransformer GenerateTransformer(bool isClient, bool isRtp)
        {
            SrtpTransformEngine engine = null;
            if (!isClient)
            {
                engine = new SrtpTransformEngine(GetMasterServerKey(), GetMasterServerSalt(), GetSrtpPolicy(), GetSrtcpPolicy());
            }
            else
            {
                engine = new SrtpTransformEngine(GetMasterClientKey(), GetMasterClientSalt(), GetSrtpPolicy(), GetSrtcpPolicy());
            }

            if (isRtp)
            {
                return engine.GetRTPTransformer();
            }
            else
            {
                return engine.GetRTCPTransformer();
            }
        }

        #endregion

        #region RTP Public Functions

        public byte[] UnprotectRTP(byte[] packet, int offset, int length)
        {
            try
            {
                return this._srtpDecoder.ReverseTransform(packet, offset, length);
            }
            catch (Exception)
            {
            }
            return null;
        }

        public int UnprotectRTP(byte[] payload, int length, out int outLength)
        {
            var result = UnprotectRTP(payload, 0, length);
            if (result == null)
            {
                outLength = 0;
                return -1;
            }

            System.Buffer.BlockCopy(result, 0, payload, 0, result.Length);
            outLength = result.Length;

            return 0; //No Errors
        }

        public byte[] ProtectRTP(byte[] packet, int offset, int length)
        {
            try
            {
                return this._srtpEncoder.Transform(packet, offset, length);
            }
            catch (Exception)
            {
            }
            return null;
        }

        public int ProtectRTP(byte[] payload, int length, out int outLength)
        {
            var result = ProtectRTP(payload, 0, length);
            if (result == null)
            {
                outLength = 0;
                return -1;
            }

            System.Buffer.BlockCopy(result, 0, payload, 0, result.Length);
            outLength = result.Length;

            return 0; //No Errors
        }

        #endregion

        #region RTCP Public Functions

        public byte[] UnprotectRTCP(byte[] packet, int offset, int length)
        {
            try
            {
                return this._srtcpDecoder.ReverseTransform(packet, offset, length);
            }
            catch (Exception)
            {
            }
            return null;
        }

        public int UnprotectRTCP(byte[] payload, int length, out int outLength)
        {
            var result = UnprotectRTCP(payload, 0, length);
            if (result == null)
            {
                outLength = 0;
                return -1;
            }

            System.Buffer.BlockCopy(result, 0, payload, 0, result.Length);
            outLength = result.Length;

            return 0; //No Errors
        }

        public byte[] ProtectRTCP(byte[] packet, int offset, int length)
        {
            try
            {
                return this._srtcpEncoder.Transform(packet, offset, length);
            }
            catch (Exception)
            {
            }
            return null;
        }

        public int ProtectRTCP(byte[] payload, int length, out int outLength)
        {
            var result = ProtectRTCP(payload, 0, length);
            if (result == null)
            {
                outLength = 0;
                return -1;
            }

            System.Buffer.BlockCopy(result, 0, payload, 0, result.Length);
            outLength = result.Length;

            return 0; //No Errors
        }

        #endregion

        #region Datagram Transport Implementations

        protected bool HasTimeout()
        {
            return this.startTime == System.DateTime.MinValue || (System.DateTime.Now - this.startTime).TotalMilliseconds > MAX_DELAY;
        }

        public int GetReceiveLimit()
        {
            return this.receiveLimit;
        }

        public int GetSendLimit()
        {
            return this.sendLimit;
        }

        public void WriteToRecvStream(byte[] buf)
        {
            lock (_inStream)
            {
                _inStream.Write(buf, 0, buf.Length);
            }
        }

        public int Receive(byte[] buf, int off, int len, int waitMillis)
        {
            //Try Solver as Socket
            if (_socketContext != null)
            {
                return ReceiveAsSocket(buf, off, len, waitMillis);
            }

            if (this.HasTimeout())
            {
                Close();
                throw new System.Exception("Handshake is taking too long! (>" + MAX_DELAY + "ms");
            }

            if (_inStream.Position <= 0)
            {
                Task.Delay(waitMillis).Wait();
            }

            if (_inStream.Position > 0)
            {
                lock (_inStream)
                {
                    var msBuf = _inStream.ToArray();
                    Buffer.BlockCopy(msBuf, 0, buf, off, msBuf.Length);
                    _inStream.Position = 0;
                    return msBuf.Length;
                }
            }

            return -1;
        }

        public void Send(byte[] buf, int off, int len)
        {
            if (!this.HasTimeout())
            {
                if (_socketContext != null)
                {
                    SendAsSocket(buf, off, len);
                }
                if (len > 0)
                {
                    OnDataReady?.Invoke(buf.Skip(off).Take(len).ToArray());
                }
            }
        }

        public virtual void Close()
        {
            this._socketContext = null;
            this.startTime = System.DateTime.MinValue;
            this._remoteEndPoint = null;
        }

        #endregion

        #region Datatram Transport as Socket Functions

        protected virtual void SetSocketContent(Socket socket, IPEndPoint targetEndPoint = null)
        {
            _socketContext = socket;
            _remoteEndPoint = targetEndPoint;
        }

        protected virtual int ReceiveAsSocket(byte[] buf, int off, int len, int waitMillis)
        {
            if (_socketContext == null)
                return -1;

            //Handshake reliable contains too long default backoff times
            waitMillis = System.Math.Max(100, waitMillis / (random.Next(100, 1000)));

            var readStartTime = System.DateTime.Now;
            var currentWaitTime = waitMillis;
            var totalReceivedLen = 0;

            while (currentWaitTime > 0)
            {
                if (this.HasTimeout())
                {
                    Close();
                    throw new System.Exception("Handshake is taking too long! (>" + MAX_DELAY + "ms");
                }
                if (!_socketContext.IsBound)
                {
                    Close();
                    throw new System.Exception("Socked not bound to any IP Address");
                }

                var cachedTimeout = _socketContext.ReceiveTimeout;
                try
                {
                    _socketContext.ReceiveTimeout = currentWaitTime;
                    // Creates an IPEndPoint to capture the identity of the sending host.
                    // In UDP we dont have Connect/Accept rule, so we must listen to packets to know the sender
                    if (_remoteEndPoint == null || _remoteEndPoint.AddressFamily != _socketContext.AddressFamily)
                        _remoteEndPoint = new IPEndPoint(_socketContext.AddressFamily == AddressFamily.InterNetwork ? IPAddress.Any : IPAddress.IPv6Any, 0);
                    
                    EndPoint senderRemote =  (EndPoint)_remoteEndPoint;

                    if (!_socketContext.IsBound)
                        _socketContext.Bind(new IPEndPoint(IPAddress.IPv6Any, 0));

                    var receivedLength = 0;
                    if (off == 0)
                    {
                        receivedLength = _socketContext.ReceiveFrom(buf, len, SocketFlags.None, ref senderRemote);
                    }
                    else
                    {
                        byte[] rv = new byte[len];
                        receivedLength = _socketContext.ReceiveFrom(rv, len, SocketFlags.None, ref senderRemote);

                        if (receivedLength > 0)
                            Buffer.BlockCopy(rv, 0, buf, off, receivedLength);
                    }

                    //Set IPEndPoint from received message
                    _remoteEndPoint = senderRemote as IPEndPoint;

                    //Update offset and received length
                    off += receivedLength;
                    totalReceivedLen += receivedLength;
                    //Reduce the receive limit to prevent errors in next run
                    len -= receivedLength;
                }
                catch (System.Net.Sockets.SocketException) { }
                finally
                {
                    if (_socketContext != null)
                    {
                        _socketContext.ReceiveTimeout = cachedTimeout;
                    }
                }
                System.Threading.Thread.Sleep(1);
                currentWaitTime = waitMillis - (int)(System.DateTime.Now - readStartTime).TotalMilliseconds;
            }

            return totalReceivedLen > 0 ? totalReceivedLen : -1;
        }

        protected virtual void SendAsSocket(byte[] buf, int off, int len)
        {
            if (_socketContext == null)
                return;

            if (!HasTimeout())
            {
                if (_socketContext != null && this._remoteEndPoint != null)
                {
                    if (_remoteEndPoint.AddressFamily == AddressFamily.InterNetwork && _socketContext.AddressFamily != _remoteEndPoint.AddressFamily)
                        _remoteEndPoint = new IPEndPoint(_remoteEndPoint.Address.MapToIPv6(), _remoteEndPoint.Port);

                    _socketContext.SendTo(buf, off, len, SocketFlags.None, _remoteEndPoint);
                    System.Threading.Thread.Sleep(1);
                }
            }
        }

        #endregion
    }
}
