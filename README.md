# KyubCryptoLibs

This repository contains two projects:
* BouncyCastle for Unity
* DtlsSrtp BouncyCastle Extension


Install Guide:

Open Package/manifest.json in your UnityProject and add following lines

```
{
  "dependencies": {
    "kyub.org.bouncycastle": "https://gitlab.com/KyubInteractive/unitybouncycastle.git#kyub.org.bouncycastle-1.8.6",
    "kyub.org.bouncycastle.dtlssrtp": "https://gitlab.com/KyubInteractive/unitybouncycastle.git#kyub.org.bouncycastle.dtlssrtp-1.0.0",
    //Other packages...
    }
}
        
```
